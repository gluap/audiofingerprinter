#!/usr/bin/python
# I know, this needs a lot more work!

import numpy
import pickle
import time
from optparse import OptionParser

import fingerprinter
import pysrt

parser = OptionParser()
parser.add_option("-f", "--fingerprintfile", dest="fingerprintfile",
                  help="Fingerprint file", metavar="FILE")
parser.add_option("-s", "--srtfile", dest="srtfile",
                  help="srt file")

(options, args) = parser.parse_args()
srtfile = ""
audiofile = ""
if options.srtfile:
    srtfile = options.srtfile
if options.fingerprintfile:
    fingerprintfile = options.fingerprintfile
else:
    raise Exception("need input fingerprint file name (-f)")

test = pickle.load(open(fingerprintfile, "rb"))
starttime = time.time()
audiodata = fingerprinter.record(5.0)
test2 = fingerprinter.Fingerprint(audiodata=audiodata, coverage_factor=3, PrintSeconds=4096. / 44100)

srt = pysrt.open(srtfile)

import subprocess

print "trying better finding"
(i, dist) = test.find_in(test2.bfingerprint)
timedelta = time.time() - starttime
print "bestfit %d %d" % (i, dist)
print "timecode %f" % test.times[i]
now = time.time() - starttime + test.times[i]
print now
subprocess.call("mplayer -ss %f test.wav" % now, shell=True)
t0 = test.times[i]
for i in numpy.arange(0, 1000):
    timedelta = time.time() - starttime
    movietime = timedelta + t0 + .1
    for subtitle in srt:
        if (subtitle.start.ordinal < movietime * 1000 and subtitle.end.ordinal > movietime * 1000):
            print subtitle.text
    print movietime
    time.sleep(0.1)
