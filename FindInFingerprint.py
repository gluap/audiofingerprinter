#!/usr/bin/python
# I know, this needs a lot more work!

import numpy
import pickle
import wave
from optparse import OptionParser

from fingerprinter import Fingerprint

parser = OptionParser()
parser.add_option("-a", "--audiofile", dest="audiofile",
                  help="write report to FILE", metavar="FILE")
parser.add_option("-s", "--srtfile", dest="srtfile",
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()
srtfile = ""
audiofile = ""
if options.srtfile:
    srtfile = options.srtfile
if options.audiofile:
    audiofile = options.audiofile
else:
    raise Exception("need input file name (-a)")

wav = wave.open(audiofile)
test2 = Fingerprint.Fingerprinter(audiodata=numpy.fromstring(wav.readframes(1000000), "<i2"), overlap=3)
test = pickle.load(open("test.wav.pickle", "rb"))
print "trying better finding"
timecode = test.find_in(test2)
print "bestfit timecode %f" % timecode
