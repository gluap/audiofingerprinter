import time
from kivy.app import App
from kivy.clock import Clock
from kivy.uix.widget import Widget

from kivy.properties import StringProperty, NumericProperty, \
    ObjectProperty


class Subtitle(Widget):
    subtitle = StringProperty("test")
    starttime = NumericProperty(0)
    dt = NumericProperty(0.0)
    srt = ObjectProperty(None)

    def update(self, dt):
        # print self.subtitle
        print dt
        currtime = time.time() - self.starttime - self.dt
        self.subtitle = str(currtime)
        print self.subtitle


class MySubtitleApp(App):
    def build(self):
        main = Subtitle()
        Clock.schedule_interval(main.update, 1.0)
        return main


if __name__ == '__main__':
    MySubtitleApp().run()
