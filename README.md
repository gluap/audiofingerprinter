# Audio fingerprinter #

This project is a proof of concept for temporal alignment of subtitles with the audio of
a movie played on another device. The alignment is based on a precomputed fingerprint
of the audio track which is then synchronized to audio recorded by microphone on
the device which will display the subtitles. The fingerprint is sufficiently robust to
work on different language versions of a movie (provided they have the same effects
soundtrack) and can deal with the noise involved when recording audio on a mobile device.

The motivation behind this project ist to enable people to bring their own subtitles to
the movie theater (coming in handy for the deaf or people watching movies in foreign countries).

A possible extension is to bring your own audio to also listen to the audio in a different
language. However the second is significantly more challenging as it requires synchronization
of with a much smaller offset in time than displaying subtitles does.

See a video of an early version of the project in action at https://youtu.be/57XTWzpFkeQ

The example files (``test.wav``, ``test.mp3``, ``test.srt`` as well as the files in 
``fingerprinter/tests/data``) are from the creative commons movie Tears of Steel.
 
 Tears of Steel is **(CC) Blender Foundation | [mango.blender.org](http://mango.blender.org)**
 
 
## Dependencies ##

To use this software you need:

**[pysrt](https://pypi.python.org/pypi/pysrt)**

To parse subtitle files in srt format. Can be installed via 

    pip install pysrt


**[kivy](http://kivy.org/)**

To display the GUI. Can be installed via 
    
    pip install kivy 

but you might prefer ready-made packages for your system as kivy requires different
other packages to be installed for building it.


**[pyaudio](http://people.csail.mit.edu/hubert/pyaudio/)**

To record a short snippet of audio to compare with the movie fingerprint and calculate
the current running time. Can be installed via 

    pip install pyaudio
but you might prefer ready-made packages for your system. See the page of pyaudio for
more information


## Optional Dependencies ##

**[VLC-Player](http://videolan.org)**
 
to have the audio automatically extracted from your video file or
converted from another audio format via ``fingerprint_file.py``.  To
directly generate the fingerprint corresponding to a video file use
(``-v <path-to-your-videofile>``). For the script to work the VLC player needs to be on your path.