fingerprinter
*************
Module for calculation and comparison of audio fingerprints.

.. contents::


fingerprinter
^^^^^^^^^^^^^
Representation and handling of fingerprints.

.. automodule:: fingerprinter.fingerprint
:members:

audiosource
^^^^^^^^^^^
Architecture-independent rappers to handle different sources of audiodata alike.

.. automodule:: fingerprinter.audiosource
:members:
