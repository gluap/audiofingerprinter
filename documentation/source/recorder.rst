audiorecorder
--------------------
Wrapper around different audio recorder methods for different systems (right now supports
:pypi:`PyAudio` and kivys own audiostream

.. contents::


.. automodule:: recorder.recorder
:members:
