#!/usr/bin/python
# I know, this needs a lot more work!

import os
import sys
from optparse import OptionParser

from fingerprinter import extract_audio
from fingerprinter.fingerprint import Fingerprint

parser = OptionParser()
parser.add_option("-a", "--audiofile", dest="audiofile",
                  help="fingerprint audiofile ", metavar="FILE")
parser.add_option("-s", "--srtfile", dest="srtfile",
                  help="use this srtfile", metavar="FILE")
parser.add_option("-v", "--videofile", dest="videofile",
                  help="fingerprint audio from video file, overrides audiofile", metavar="FILE")
(options, args) = parser.parse_args()
srtfile = ""
audiofile = ""
videofile = False
if options.videofile:
    options.audiofile = ("%s.wav" % os.path.splitext(os.path.basename(options.videofile))[0])
    videofile = options.videofile
if options.srtfile:
    srtfile = options.srtfile
if options.audiofile:
    audiofile = options.audiofile
else:
    raise Exception("need input file name (-a)")

sys.argv = sys.argv[0:1]

if videofile:
    extract_audio(videofile, options.audiofile)

test = Fingerprint(audiofile, coverage_factor=3)  # 2048/44100.)
print test.bfingerprint[0:5]
test.dump_to("%s.pickle" % audiofile)
print("%s.pickle" % audiofile)[-6:]
test2 = Fingerprint("%s.pickle" % audiofile)
print test2.bfingerprint[0:5]
