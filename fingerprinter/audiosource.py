# encoding: utf-8
from __future__ import unicode_literals, absolute_import

import numpy
import wave


class AudioSource(object):
    """
    Parent class for different audio sources
    """
    framerate = None

    def __init__(self, type):
        self.type = type

    def get_n(self, n):
        """
        Get the next ``n`` samples of audio.

        :param int n:
        :return numpy.array: ``n`` samples
        """
        raise NotImplementedError()

    def seek(self, n):
        """
        Seek to frame :math:`n`
        :param int n:
        """
        raise NotImplementedError()

    def seek_to_second(self, seconds):
        """
        Seek to a specified time (in seconds)
        :param seconds:
        :return:
        """
        self.seek(int(seconds * self.framerate))

    def get_seconds(self, seconds):
        """
        Return the specified number of seconds worth of frames
        :param seconds:
        :return:
        """
        return self.get_n(seconds * self.framerate)


class ArraySource(AudioSource):
    """
    Class for handling audio data stored in an array.
    feed an array of numbers as input data

    :param basestring data: a byte array
    """
    framerate = 22050

    def __init__(self, data):
        """

        """
        self.data = data.copy()
        self.i = 0
        self.frames = len(data)

    def get_n(self, n):
        """
        Get the next ``n`` samples of audio.

        :param int n:
        :return numpy.array: ``n`` samples
        """
        endindex = min(self.i + n, len(self.data))
        returndata = self.data[self.i:endindex]
        self.i = endindex
        return returndata

    def seek(self, n):
        self.i = n


class FileSource(AudioSource):
    """
    Class for handling audio data from a file

    Reads audio data from a file

    :param unicode filename: the filename of the audio data to be read
    """

    def __init__(self, filename):
        """

        """
        self.audio = wave.open(filename)
        self.framerate = self.audio.getframerate()
        self.frames = self.audio.getnframes()
        print "file has framerate %d" % self.framerate
        if self.audio.getnchannels() > 1:
            raise AudioFileError("Error: Need a monaural wav input for the time being")
        if self.audio.getsampwidth() != 2:
            raise AudioFileError("Error: Need a 16 bit wav input for the time being")
        super(FileSource, self).__init__("filesource")

    def get_n(self, n):
        """
        Get the next ``n`` samples of audio.

        :param int n:
        :return numpy.array: ``n`` samples
        """
        return numpy.frombuffer(self.audio.readframes(n), dtype="i2")

    def seek(self, n):
        """
        Seek to frame :math:`n`
        :param int n:
        """
        if n > self.frames:
            raise AudioFileError("cannot seek to frame {}: beyond length of audio {}".format(n, self.frames))
        self.audio.rewind()
        self.audio.readframes(n)


class AudioFileError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
