# encoding: utf-8
from __future__ import unicode_literals, absolute_import

from fingerprinter.audiosource import ArraySource, FileSource

"""
Audio Fingerprinter Module
=====================
"""

import numpy
import pickle


class Fingerprint(object):
    """
    This class manages and compares fingerprints. The main functionality is calculating the fingerprint of a longer
    audio track and finding the likely time corresponding to a shorter fingerprint calculated for a shorter piece
    of audio.

    Should be initialized either from a .pickle file produced via the dumpTo method,
    via audiodata (as a string of 22050 khz mono audio data) or from a wav file (22050 khz mono)

    :param str filename:
    :param fingerprinttype: Not used
    :param print_seconds: Length of audio in seconds
    :param int coverage_factor: coverage factor, for a value of 1, each sample is part of one spectrum. For a value
                            of 2 the first samples :math:`0\cdots 2n` are part of the first frequency sample,
                            :math:`n\cdots 3n` are part of the second etc.
    :param audiodata: a byte stream of audio data. Expects 22khz 16bit mono
    """
    boundaries = numpy.floor(20 * numpy.exp(numpy.arange(1, 33) / 31. * 5.70378247 / numpy.log(8.78)))
    powers = 2 ** numpy.arange(0, 31)

    def __init__(self, filename=None, fingerprinttype=1, print_seconds=(2048 / 44100.), coverage_factor=3,
                 audiodata=None):


        self.framerate = None
        self.source = None;
        self.samples_per_line = None
        self.frames = None
        self.fingerprint = None
        self.bfingerprint = None
        if audiodata is None and filename[-6:] == "pickle":
            self.init_from_pickle(filename)
        else:
            self.type = fingerprinttype
            self.coverage_factor = coverage_factor
            self.PrintSeconds = print_seconds
            if (filename is None):
                self._init_from_data(audiodata)
            else:
                self._init_from_file(filename)

    def _init_from_data(self, audiodata):
        self.source = ArraySource(audiodata)
        self._generate_fingerprint()

    def _init_from_file(self, filename):
        self.source = FileSource(filename)
        self._generate_fingerprint()

    def _generate_fingerprint(self):
        self.framerate = self.source.framerate
        self.samples_per_line = int(numpy.floor(self.PrintSeconds * self.framerate))
        self.generate_logarithmic_boundaries()
        self.frames = self.source.frames / self.samples_per_line
        self.bfingerprint = numpy.zeros((self.frames, 31), dtype=numpy.bool)
        self.fingerprint = numpy.zeros(self.frames, dtype="i4")
        # current_sum = numpy.zeros(31, dtype="i4")
        # last_sum = numpy.zeros(31, dtype="i4")
        temp_frame = numpy.zeros(self.coverage_factor * self.samples_per_line)
        temp_frame[0:self.coverage_factor * self.samples_per_line] = self.source.get_n(
            self.coverage_factor * self.samples_per_line)
        last_spectrum = self._spectrum(temp_frame)
        #last_sum = self.sum_spectrum(last_spectrum)
        current_spectrum = self._spectrum(temp_frame)
        current_sum = self.sum_spectrum(current_spectrum)
        next_buffer = self.source.get_n(self.samples_per_line)
        i = 0
        while len(next_buffer) == self.samples_per_line:
            temp_frame[0:(self.coverage_factor - 1) * self.samples_per_line] = temp_frame[self.samples_per_line:]
            temp_frame[(self.coverage_factor - 1) * self.samples_per_line:] = next_buffer
            last_sum = current_sum.copy()
            current_spectrum = self._spectrum(temp_frame)
            current_sum = self.sum_spectrum(current_spectrum)
            self.bfingerprint[i] = (current_sum[0:-1] - current_sum[1:] > last_sum[0:-1] - last_sum[1:])
            self.fingerprint[i] = numpy.sum(self.powers * self.bfingerprint[i])
            next_buffer = self.source.get_n(self.samples_per_line)
            i += 1
        self.bfingerprint = self.bfingerprint[0:i]
        self.fingerprint = self.fingerprint[0:i]

    def sum_spectrum(self, spectrum):
        temp = numpy.zeros(len(self.boundaries), dtype="i8")
        for j in numpy.arange(0, len(self.boundaries) - 1):
            temp[j] = numpy.sum(spectrum[self.boundaries[j]:self.boundaries[j + 1]])
            if temp[j] < 0:
                raise Exception("Overflow of spectrum sum. Spec minimum %f %f" % (spectrum[0], spectrum[1]))
        return temp

    def _spectrum(self, data):
        return numpy.abs(numpy.fft.rfft(numpy.hamming(self.coverage_factor * self.samples_per_line) * data)[0:300]) ** 2

    def _distance(self, compfingerprint, offset, size):
        length_sum = min(size, len(compfingerprint.bfingerprint))
        return numpy.sum(numpy.logical_xor(self.bfingerprint[offset:offset + length_sum],
                                           compfingerprint.bfingerprint[0:length_sum]))

    def bdistance(self, bfingerprint, offset):
        return numpy.sum(numpy.logical_xor(self.bfingerprint[offset:offset + len(bfingerprint)], bfingerprint))

    def find_in(self, compfingerprint):
        """
        Find the (shorter) fingerprint ``compfingerprint`` in this fingerprint

        :param Fingerprint compfingerprint: The fingerprint to locate
        :return: A time code in seconds
        """
        current_minimum = 10000000
        best_fit = 0
        values = numpy.zeros(len(self.bfingerprint) - len(compfingerprint.bfingerprint))
        for i in numpy.arange(0, len(self.bfingerprint) - len(compfingerprint.bfingerprint)):
            values[i] = self._distance(compfingerprint, i, 40)
        sorter = values.argsort()
        # print "10 lowest distances (full sample):"
        for i in sorter[0:20]:
            temp = 0
            self._distance(compfingerprint, i, len(compfingerprint.bfingerprint) - 1)
            if temp < current_minimum:
                current_minimum = temp
                best_fit = i
                # print "time {} value {} prev {} i {} ".format(
                #    self._get_time(i + len(compfingerprint.bfingerprint)), temp, values[i], i)
        return self._get_time(best_fit + len(compfingerprint.bfingerprint))

    def _get_time(self, i):
        return float(1. * self.samples_per_line / self.framerate * i)

    def generate_logarithmic_boundaries(self):
        """
        We want to have boundaries that are "logarithmically spaced".
        However, at a sample length of 512 samples for 22050hz sample rate
        real logarithmic spacing gives a resolution below 32 bits because
        the lowest bins are less than one frequency channel wide.
        
        Thus we decided to instead space the channels according to the equation:
        :math:`\exp(a+0b)-n==f_0` (lower boundary frequency for channel 0)
        :math:`\exp(a+16b)-n==f_1` (upper boundary frequency for channel 15)
        """
        fftfreqs = numpy.fft.fftfreq(self.samples_per_line, 1. / 22050)
        fstep = fftfreqs[1] - fftfreqs[0]
        f0 = 200. / fstep  # we want to start at 200 hertz. Frequency chosen so mic cuttof does not affect us
        f1 = 2000. / fstep  # we want to cut off at 2000 hz
        n = 16.  # empirical value for n to get reasonable channel width
        a = numpy.log(f0 + n)
        b = -1. / 32. * numpy.log((f0 + n) / (f1 + n))
        self.boundaries = map(int, numpy.exp(a + b * numpy.arange(0, 32)) - n)

    def init_from_pickle(self, filename):
        """
        Inits from a pickled object

        :param unicode filename: the filename to initialize the fingerprint from
        """
        self.__dict__ = pickle.load(file(filename, 'rb')).__dict__
        self.init_bfingerprint_from_int()

    def init_bfingerprint_from_int(self):
        """
        Reads the binary data from fingerprint and generates the bfingerprint
        necessary to avoid big fingerprint files (unfortunately, numpy uses 8 bytes
        for storage of booleans in a boolean array.
        """
        self.bfingerprint = numpy.array(map(lambda x: ((self.powers & x) > 0), self.fingerprint))

    def dump_to(self, filename):
        """
        dump the fingerprint to a pickle

        :param unicode filename: the filename to dump the fingerprint to
        """
        self.bfingerprint = ""
        pickle.dump(self, file(filename, 'wb'), 2)


