# encoding: utf-8
from __future__ import unicode_literals, absolute_import

from nose.tools import with_setup
from random import random, seed

from fingerprinter.fingerprint import Fingerprint, FileSource

seed("my butt hurtssss")
audiosource = FileSource("data/tears_of_steel_720p.wav")
audiosource_rerecorded = FileSource("data/rerecorded.wav")
initialdelta = 2.88  # time shift between recording and original audio in seconds


def get_five_seconds(audiosource, time):
    audiosource.seek_to_second(time)
    return audiosource.get_seconds(5)


def setup4():
    global reference_fingerprint
    reference_fingerprint = Fingerprint("data/tears_of_steel_720p.wav", coverage_factor=4)


def setup3():
    global reference_fingerprint
    reference_fingerprint = Fingerprint("data/tears_of_steel_720p.wav", coverage_factor=3)


def setup2():
    global reference_fingerprint
    reference_fingerprint = Fingerprint("data/tears_of_steel_720p.wav", coverage_factor=2)


def setup1():
    global reference_fingerprint
    reference_fingerprint = Fingerprint("data/tears_of_steel_720p.wav", coverage_factor=1)


random_locations = [3 + random() * (audiosource_rerecorded.frames / audiosource.framerate - 9) for i in range(100)]


@with_setup(setup2)
def test_2():
    for i in random_locations:
        yield check_overlap, i, 2


def check_location(time):
    data = get_five_seconds(audiosource, time)
    testfingerprint = Fingerprint(audiodata=data)
    seek_finds = reference_fingerprint.find_in(testfingerprint) - 5
    assert abs(time - seek_finds) < 0.2, "time is {}, reference returns {}".format(time, seek_finds)


def check_overlap(time, overlap):
    data = get_five_seconds(audiosource_rerecorded, time)
    testfingerprint = Fingerprint(audiodata=data, coverage_factor=overlap)
    seek_finds = reference_fingerprint.find_in(testfingerprint) - 5
    delta = abs(time - seek_finds - initialdelta)
    assert delta < 0.2, "delta is {} time is {}, reference returns {}".format(delta, time, seek_finds)
