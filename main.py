# encoding: utf-8
from __future__ import unicode_literals, absolute_import

import pysrt
from kivy.app import App
from kivy.clock import Clock
from kivy.core.audio import SoundLoader, Sound
from kivy.logger import Logger
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.widget import Widget

from kivy.properties import NumericProperty, ReferenceListProperty, \
    ObjectProperty, StringProperty, ListProperty
# import pickle
import fingerprinter
import recorder
import time
# Config.set('graphics', 'fullscreen', 'auto')
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.factory import Factory
from kivy.uix.popup import Popup
# from kivy import get_input
import os.path
from chardet.universaldetector import UniversalDetector
import threading

__version__ = "0.1"

Logger.setLevel("DEBUG")


class RealignButton(Button):
    text = StringProperty("lala")


class Subtitle(Widget):
    text = StringProperty("test")
    x = ObjectProperty(400)
    y = ObjectProperty(300)
    center = ReferenceListProperty(x, y)
    width = ObjectProperty(800)
    height = ObjectProperty(600)
    wh = ReferenceListProperty(width, height)
    font_size = NumericProperty(40)
    dt = NumericProperty(0.0)
    t0 = NumericProperty(time.time())
    filename = StringProperty(None)
    srt = ObjectProperty(None)
    fingerprint = ObjectProperty(None)
    status = StringProperty("status")
    ComputeThread = ObjectProperty(None)
    movietime = NumericProperty(None)
    audio_on = False
    audio = None
    do_seek = False
    running = False
    lastseek = 0
    guiDt = 0.
    lastGuiDt = 0.

    def show(self, str, x=400, y=300, width=640, height=480):
        if self.running:
            self.findcurrent()
        else:
            self.text = "." * int(time.time() % 4) + "loading " + "." * int(time.time() % 4)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        # Logger.debug("width= %d" % width)

    def parse_fingerprint(self, filename):
        fingerprint = fingerprinter.Fingerprint(filename + ".pickle")
        detected = detect_encoding(filename + ".srt")
        srt = pysrt.open(filename + ".srt", encoding=detected['encoding'])
        self.fingerprint = fingerprint
        self.srt = srt
        self.running = True
        return

    def load_file(self, filename):
        Logger.info("loading srt for {}".format(filename))
        threading.Thread(target=self.parse_fingerprint, args=(filename,)).start()

    def init(self):
        self.audiorecorder = recorder.Audiorecorder()

    def set_file(self, filename):
        self.running = False
        self.filename = filename
        self.load_file(filename)

    def findcurrent(self):
        current = ""
        self.movietime = time.time() - self.t0 + self.dt + self.guiDt
        for subtitle in self.srt:
            if subtitle.start.ordinal < self.movietime * 1000 < subtitle.end.ordinal:
                current = subtitle.text
        self.text = current
        self.movietime = time.time() - self.t0 + self.dt + self.guiDt
        if self.guiDt != self.lastGuiDt:
            self.do_seek = True

        if self.do_seek and (abs(time.time() - self.lastseek) > 2.) and self.audio_on:
            self.lastseek = time.time()
            modtime = (time.time() - self.t0 + self.dt + self.guiDt) % 1
            Clock.schedule_once(self.jump_to_movie_time, 1. - modtime + .01)

    def jump_to_movie_time(self, *args):
        if abs(self.audio.get_pos() - self.movietime) < .1:
            Logger.debug("audio is playing from correct position")
            self.do_seek = False
        else:
            Logger.debug("Seeking not successful, still %f seconds off" % (abs(self.audio.get_pos() - self.movietime)))
            Logger.debug("Audio position is %f" % (self.audio.get_pos()))
            Logger.debug("seeking to %f" % self.movietime)
            Logger.debug("Delta t is set to %f" % (self.guiDt))
            self.audio.seek(self.movietime)

    def relocate(self, *args):
        threading.Thread(target=self._relocate, args=()).start()

    def _relocate(self):
        self.running = False
        self.status = "finding position"
        Logger.debug("starting position finder thread")
        Logger.debug("recording audio data")
        recorded_data = self.audiorecorder.get_data()
        self.t0 = time.time()

        Logger.debug("computing audio location (%f seconds for getdata)" % (time.time() - self.t0))

        self.dt = -10000
        recorded_fingerprint = fingerprinter.Fingerprint(audiodata=recorded_data, coverage_factor=3)
        self.dt = self.fingerprint.find_in(recorded_fingerprint)
        Logger.debug("found time at timecode %f" % self.dt)
        self.status = "found position: %f" % self.dt
        self.running = True
        if self.audio_on:
            self.audio.play()
            self.do_seek = True

    def toggle_audio(self, *args):
        if self.audio is not None:
            self.audio_on = not self.audio_on
            Logger.info("audio toggled {}".format(self.audio_on))

            if self.audio_on:
                self.audio.play()
                self.do_seek = True
            else:
                self.audio.stop()


class PongGame(Screen):
    is_loading = False
    sub = ObjectProperty(None)
    buttonRealign = ObjectProperty(None)
    buttonToggleAudio = ObjectProperty(None)

    def update(self, dt):
        if self.is_loading:
            self.sub.show("loading file", x=self.center_x, y=self.center_y, width=self.width, height=self.height)
        else:
            self.sub.show(str(dt), x=self.center_x, y=self.center_y, width=self.width, height=self.height)

    def init_filename(self, filenamebase):
        self.is_loading = True
        self.sub.text = "loading %s" % filenamebase
        Logger.debug("loading fingerprint file %s" % filenamebase)
        self.sub.set_file(filenamebase)
        if os.path.isfile("%s.mp3" % filenamebase):
            Logger.info("loading %s.mp3" % filenamebase)
            if isinstance(self.sub.audio, Sound):
                self.sub.audio.stop()
                self.sub.audio.unload()
            self.sub.audio = SoundLoader.load("%s.mp3" % filenamebase)
            self.sub.audio_on = False
        else:
            self.sub.audio = None
        self.is_loading = False

    def init(self):
        self.buttonRealign.bind(on_release=self.sub.relocate)
        self.buttonAudio.bind(on_release=self.sub.toggle_audio)
        self.sub.init()


class LoadDialog(FloatLayout):
    path = StringProperty("/")
    filters = ListProperty(None)
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class SettingsScreen(Screen):
    def show_load(self, target):
        self.target = target
        mypath = os.path.abspath(os.path.dirname(__file__))
        Logger.debug("my path is {}".format(mypath))
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup, path=mypath, filters=["*.pickle"])
        self._popup = Popup(title="Load file", content=content, size_hint=(0.9, 0.9))
        self._popup.open()

    def dismiss_popup(self):
        self._popup.dismiss()

    def load(self, path, filename):
        self.dismiss_popup()
        filenamebase = filename[0][0:-7]
        self.target.init_filename(filenamebase)
        self.parent.current = 'subtitle'


class PongApp(App):
    def build(self):
        sm = ScreenManager()
        self.game = PongGame(name="subtitle")
        self.game.init_filename("test")
        self.game.init()
        self.settings = SettingsScreen(name="settings")
        Clock.schedule_interval(self.game.update, 1. / 25.)
        sm.add_widget(self.game)
        sm.add_widget(self.settings)
        Logger.debug("now it is starting")
        return sm


def detect_encoding(filename):
    detector = UniversalDetector()
    detector.reset()
    for line in file(filename, 'rb'):
        detector.feed(line)
        if detector.done:
            break
    detector.close()
    return detector.result


Factory.register('LoadDialog', cls=LoadDialog)
if __name__ == '__main__':
    PongApp().run()
