# encoding: utf-8
from __future__ import unicode_literals, absolute_import

import numpy
import wave
from kivy.clock import Clock
from kivy.logger import Logger

iface = "pyaudio"
try:
    import pyaudio
except ImportError:
    iface = "audiostream"
    try:
        from audiostream import get_input
    except ImportError:
        raise Exception("need either pyaudio or kivy audiostream working")

if iface is "audiostream":
    class Audiorecorder:

        def __init__(self, size=40 * 2048, **kwargs):
            self._data = b''
            self.maxsize = size
            self.mic = get_input(callback=self._mic_callback, buffersize=2048, rate=22050, channels=1)
            self.mic.start()
            Clock.schedule_interval(self._record_callback, 1 / 60.)

        def _mic_callback(self, buf):
            #        Logger.debug("mic callback")
            if not buf:
                return
            self._data += buf
            if len(self._data) > self.maxsize:
                self._data = self._data[-self.maxsize:]

        def _record_callback(self, *args):
            #        Logger.debug("record callback")
            self.mic.poll()

        def get_data(self):
            Logger.debug("get data")
            w = wave.open('recorded.wav', 'w')
            w.setnchannels(1)
            w.setsampwidth(2)
            w.setframerate(22050)

            w.writeframes(self._data)
            w.close()
            return numpy.fromstring(str(self._data), dtype="i2")

elif iface is "pyaudio":
    class Audiorecorder:
        def __init__(self, size=40 * 2048, **kwargs):
            self._data = b''
            self.maxsize = size
            self.pyaudio = pyaudio.PyAudio()
            self.stream = self.pyaudio.open(format=self.pyaudio.get_format_from_width(2),
                                            channels=1,
                                            rate=22050,
                                            input=True,
                                            frames_per_buffer=1024,
                                            stream_callback=self._mic_callback)
            self.stream.start_stream()

        def _mic_callback(self, in_data, frame_count, time_info, status):
            if not in_data:
                return
            self._data += in_data
            if len(self._data) > self.maxsize:
                self._data = self._data[-self.maxsize:]
            return in_data, pyaudio.paContinue

        def get_data(self):
            Logger.debug("get data")
            w = wave.open('recorded.wav', 'w')
            w.setnchannels(1)
            w.setsampwidth(2)
            w.setframerate(22050)

            w.writeframes(self._data)
            w.close()
            return numpy.fromstring(self._data, dtype="i2")

        def __del__(self):
            self.stream.stop_stream()
            self.stream.close()
            self.pyaudio.terminate()
