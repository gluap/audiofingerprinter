# encoding: utf-8
from __future__ import unicode_literals, absolute_import

import os
import subprocess
import time

import vlc


# from
# http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


if which("vlc") is None:
    raise EnvironmentError("vlc not found")
else:
    vlc_binary = which("vlc")


def extract_audio(videofile, audiofile):
    """
    run VLC to extract the audio from ``videofile`` and output it into fingerprinter-compatible format in ``audiofile``

    :param videofile: a filename
    :type videofile: unicode
    :param audiofile: a filename
    :type audiofile: unicode
    """
    assert os.path.isfile(videofile), "{} is not a file".format(videofile)
    command = "%s -I dummy --no-sout-video --sout-audio --no-sout-rtp-sap --no-sout-standard-sap --ttl=1 " \
              "--sout-keep --sout-transcode-samplerate=22050 --sout " \
              "\"#transcode{acodec=s16l,channels=1}:std{access=file,mux=wav,dst=%s}\" '%s'" \
              " vlc://quit" % (vlc_binary, audiofile, videofile)


    subprocess.call(command, shell=True)


class videofile(object):
    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise IOError("File {s} not found".format(filename))
        self.filename = filename
        self.vlc = vlc.Instance("--no-audio", "-I ncurses")
        self.vlcplayer = self.vlc.media_player_new()
        self.media = self.vlc.media_new("file://" + os.path.abspath(filename))
        # self.media.parse()
        self.vlcplayer.set_media(self.media)
        self.vlcplayer.play()
        time.sleep(0.5)
        # self.vlcplayer.stop()
        print self.media.parse()
        print self.media
        print "file://" + os.path.abspath(filename)
        # test=vlc.LP_LP_MediAtr
        print self.vlcplayer.get_title()
        print self.vlcplayer.get_fps()
        self.subtitletracks = self.vlcplayer.video_get_spu_description()
        self.audiotracks = self.vlcplayer.audio_get_track_description()
        self.vlcplayer.stop()
        print self.audiotracks


if __name__ == "__main__":
    test = videofile("/nfs/video/Movies/Merida [2012].mkv")
